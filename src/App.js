import { Form, Label, Col, FormGroup, Input,Button } from 'reactstrap';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <div className='container mt-5'>
      <Form>
        <FormGroup row>
          <Col sm={8}>
            <FormGroup row>
              <Label sm={3} for="exampleEmail">
                Họ và tên
              </Label>
              <Col sm={9}>
                <Input
                  id="name"
                  name="username"
                  placeholder="with a placeholder"
                  type="text"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={3} for="exampleEmail">
                Ngày sinh
              </Label>
              <Col sm={9}>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="with a placeholder"
                  type="email"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={3} for="exampleEmail">
                Số điện thoại
              </Label>
              <Col sm={9}>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="with a placeholder"
                  type="email"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={3} for="exampleEmail">
                Giới tính
              </Label>
              <Col sm={9}>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="with a placeholder"
                  type="email"
                />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={3} for="exampleEmail">
                Email
              </Label>
              <Col sm={9}>
                <Input
                  id="exampleEmail"
                  name="email"
                  placeholder="with a placeholder"
                  type="email"
                />
              </Col>
            </FormGroup>

          </Col>
          <Col sm={4}>
            <img style={{ width: "100%", height: "100%" }} alt='aa' src={require("./assets/images/diverse-crowd-people-different-ages-races_74855-5235.avif")} />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col sm={2}>
            <Label for="exampleEmail">
              Họ và tên
            </Label>
          </Col>
          <Col sm={10}>
            <Input
              id="name"
              name="username"
              placeholder="with a placeholder"
              type="textarea"
              rows='10'
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col className='d-flex justify-content-end' sm={12}>
          <Button className='m-1' color="success">
              Chi tiết
            </Button>
            <Button className='m-1' color="success">
              Kiểm tra
            </Button>
          </Col>
        </FormGroup>
        
  
      </Form>
    </div>
  );
}

export default App;
